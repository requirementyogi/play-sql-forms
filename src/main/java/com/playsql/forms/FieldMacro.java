package com.playsql.forms;


import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static com.google.common.base.Objects.equal;

/**
 * Macro for individual fields. They just serve as a placeholder and are replaced in the front-end.
 *
 * Copyright 2015 Adrien Ragot - See LICENSE.txt
 */
public class FieldMacro implements Macro {

    final private static Logger log = LoggerFactory.getLogger(FormMacro.class);
    public static final String DEFAULT_TEXT = "The Field macro must be used inside a Play SQL Form.";

    public final SoyTemplateRenderer soyTemplateRenderer;

    public FieldMacro(SoyTemplateRenderer soyTemplateRenderer) {
        this.soyTemplateRenderer = soyTemplateRenderer;
    }

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException {
        String css = "playsql-form-field";
        if (Boolean.valueOf(parameters.get("cardLayout"))) css += " card-layout";
        if (Boolean.valueOf(parameters.get("hidden"))) css += " hidden";
        String size = parameters.get("size");
        if (equal(size, "Small")) css += " field-size-small";
        else if (equal(size, "Medium")) css += " field-size-medium";
        else if (equal(size, "Large")) css += " field-size-large";

        Map<String, Object> map = Maps.newHashMap();
        map.put("type", parameters.get("type"));
        map.put("macroParams", parameters);
        map.put("css", css);

        try {
            return soyTemplateRenderer.render("com.playsql.playsql-forms-plugin:soy-templates", "Confluence.Templates.PlaySQL.fieldMacro", map);
        } catch (SoyException e) {
            log.info("Error while rendering Play SQL's 'field' macro", e);
            return "Error while rendering Play SQL's 'field' macro: " + e.getMessage();
        }
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.INLINE;
    }

}
