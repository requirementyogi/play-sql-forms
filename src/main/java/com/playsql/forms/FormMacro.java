package com.playsql.forms;


import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.ConversionContextOutputType;
import com.atlassian.confluence.content.render.xhtml.XmlEventReaderFactory;
import com.atlassian.confluence.content.render.xhtml.XmlOutputFactory;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.util.HtmlUtil;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.playsql.core.api.DynamicTableManager;
import com.playsql.core.api.HasPermission;
import com.playsql.core.api.PlaySqlConnector;
import com.playsql.core.api.PlaySqlPermissionManager;
import com.playsql.core.api.model.ConnectionDetails;
import com.playsql.core.model.AbstractResult;
import com.playsql.dialect.model.DDLColumn;
import com.playsql.spi.SpiUtils;
import com.playsql.spi.utils.PlaySqlUtils;
import com.playsql.spi2.Context;
import com.playsql.spi2.Spi2Router;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.XMLEvent;
import java.io.Closeable;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiConsumer;
import java.util.regex.Pattern;

import static com.playsql.dialect.api.Jdbc.ConnectionMode.CNX_RO;
import static com.playsql.spi.utils.PlaySqlUtils.equal;
import static com.playsql.spi.utils.PlaySqlUtils.firstNonNull;

/**
 * Macro for forms. They're just a placeholder and are replaced in the front-end.
 * Copyright 2015 Adrien Ragot - See LICENSE.txt
 */
public class FormMacro implements Macro {

    public final static Pattern WIDTH_VALIDATION = Pattern.compile("[0-9]+(%|px)?");
    private final static Logger log = LoggerFactory.getLogger(FormMacro.class);

    private final PlaySqlPermissionManager playSqlPermissionManager;
    private final SpiUtils spiUtils;
    private final Spi2Router spi2Router;
    private final PlaySqlConnector connector;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final DynamicTableManager dynamicTableManager;
    private final XmlEventReaderFactory xmlEventReaderFactory;
    private final XmlOutputFactory xmlOutputFactory;

    public FormMacro(PlaySqlPermissionManager permissionManager,
                     SpiUtils spiUtils,
                     PlaySqlConnector connector,
                     Spi2Router spi2Router,
                     SoyTemplateRenderer soyTemplateRenderer,
                     DynamicTableManager dynamicTableManager, XmlEventReaderFactory xmlEventReaderFactory, XmlOutputFactory xmlOutputFactory) {
        this.playSqlPermissionManager = permissionManager;
        this.spiUtils = spiUtils;
        this.connector = connector;
        this.spi2Router = spi2Router;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.dynamicTableManager = dynamicTableManager;
        this.xmlEventReaderFactory = xmlEventReaderFactory;
        this.xmlOutputFactory = xmlOutputFactory;
    }

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext conversionContext) throws MacroExecutionException {
        String contextString = parameters.get("context");
        if (StringUtils.isBlank(contextString)) {
            return "Error while rendering the Play SQL Form macro: No value for the 'context' field.";
        }
        Context context = Context.fromString(contextString);
        String spreadsheetId = parameters.get("spreadsheetId");
        if (StringUtils.isBlank(spreadsheetId) || equal("undefined", spreadsheetId)) {
            return "Play SQL Form macro: Please select a spreadsheet.";
        }
        String spreadsheetName = firstNonNull(parameters.get("spreadsheetName"), "");
        String macroName = conversionContext.getPropertyAsString("macroName");
        String type = equal(macroName, "playsql-form") ? "create"
                : equal(macroName, "playsql-browser") ? "view" : "view";
        String title = firstNonNull(parameters.get("title"), "Record Browser - %s -");
        boolean linkToSpreadsheet = Boolean.valueOf(firstNonNull(parameters.get("linkToSpreadsheet"), "true"));
        boolean canCreate = Boolean.valueOf(firstNonNull(parameters.get("canCreate"), "true"));
        String recordId = firstNonNull(parameters.get("recordId"), "");
        boolean displayRecordId = Boolean.valueOf(firstNonNull(parameters.get("displayRecordId"), "true"));
        String width = parameters.get("width");
        boolean cardLayout = Boolean.valueOf(firstNonNull(parameters.get("cardLayout"), "true"));
        String filter = firstNonNull(parameters.get("filter"), "");
        String filterText = firstNonNull(parameters.get("filterText"), "");

        // TODO: Fine-tune the permissions
        ConnectionDetails details = connector.getConnectionDetails(context);
        HasPermission permission = playSqlPermissionManager.hasPermission2(
                spiUtils.getLoggedInUser(),
                details,
                PlaySqlPermissionManager.Type.EDIT);
        if (!permission.hasPermission()) {
            return "Error: The form can't be shown because of restrictions (%s)".replace("%s", permission.formattedMessage());
        }

        // Set the title
        if (equal(macroName, "playsql-browser")) {
            title = HtmlUtil.htmlEncode(title);
            if (linkToSpreadsheet) {
                String anchor = "<a href=\"%1\">%2</a>"
                        .replace("%1", spi2Router.urlToEditor("spreadsheet", context, spreadsheetId).getUrl())
                        .replace("%2", spreadsheetName);
                title = title.replace("%s", anchor);
            } else {
                title = title.replace("%s", spreadsheetName);
            }
        }

        // Format the width
        if (width != null && !WIDTH_VALIDATION.matcher(width).matches()) {
            width = null;
        }

        ConversionContextOutputType outputType = convertOutputType(conversionContext.getOutputType());
        final ContentEntityObject entity = conversionContext.getEntity();
        AtomicReference<Pair<List<DDLColumn>, List<String>[]>> row = new AtomicReference<>(null);
        if (outputType != ConversionContextOutputType.DISPLAY && outputType != ConversionContextOutputType.PREVIEW
            && StringUtils.isNotBlank(spreadsheetId) && entity != null) {
            final long ceoId = entity.getId();
            final Long recordIdL = StringUtils.isNotBlank(recordId) ? Long.valueOf(recordId) : null;
            connector.execute(permission, CNX_RO, (cnx) -> {

                AbstractResult abstractResult = dynamicTableManager.getDataWithRetry(cnx, details, permission,
                    spreadsheetId, null,
                    null, ceoId, null, null, 1L, 0L, recordIdL != null ? Lists.newArrayList(recordIdL) : null, null,
                    null, false, DynamicTableManager.CacheAction.NO_CACHE);

                List<List<String>[]> data = abstractResult.getData();
                if (data != null && data.size() == 1) {
                    row.set(Pair.of(abstractResult.getTableDef().getColumns(), data.get(0)));
                }

                return abstractResult;
            });
        }
        // Set the placeholders, or replace them with the values.
        if (row.get() != null) {
            body = replaceFields(body, row.get());
        } else {
            body = body.replace(FieldMacro.DEFAULT_TEXT, "(Please wait)");
        }

        Map<String, Object> map = Maps.newHashMap();
        map.put("htmlUtil", new HtmlUtil());
        map.put("bodyHtml", body);
        map.put("contextString", contextString);
        map.put("spreadsheetId", spreadsheetId);
        map.put("spreadsheetName", spreadsheetName);
        map.put("createButton", "true");
        map.put("state", type);
        map.put("withBrowser", equal(macroName, "playsql-browser") ? true : false);
        map.put("title", title);
        map.put("showLinkToSpreadsheet", linkToSpreadsheet);
        map.put("editorUrl", spi2Router.urlToEditor("spreadsheet", context, spreadsheetId).getUrl());
        map.put("canCreate", canCreate);
        map.put("displayRecordId", displayRecordId);
        map.put("recordId", recordId);
        map.put("width", width);
        map.put("cardLayout", cardLayout);
        map.put("filter", filter);
        map.put("filterText", filterText);

        try {
            return soyTemplateRenderer.render("com.playsql.playsql-forms-plugin:soy-templates", "Confluence.Templates.PlaySQL.formMacro", map);
        } catch (SoyException e) {
            log.info("Error while rendering Play SQL's form' macro", e);
            return "Error while rendering Play SQL's 'form' macro: " + e.getMessage();
        }
    }

    private String replaceFields(String body, Pair<List<DDLColumn>, List<String>[]> row) {
        QName DIV = new QName("div");
        QName ATTR_CLASS = new QName("class");
        Map<String, String> values = Maps.newHashMap();
        for (int i = 0 ; i < row.getLeft().size() && i < row.getRight().length; i++) {
            values.put(row.getLeft().get(i).getName(), displayValueOf(row.getRight()[i]));
        }
        XMLEventFactory eventFactory = XMLEventFactory.newInstance();
        return openXmlTransformer(body, (reader, writer) -> {
            try {
                while (reader.hasNext()) {
                    XMLEvent event = reader.nextEvent();
                    writer.add(event);
                    if (event.isStartElement()
                        && equal(event.asStartElement().getName(), DIV)
                        && hasCss(ATTR_CLASS, event, "playsql-form-field")) {
                        String column = getAttr(event, "data-column");
                        String value = values.get(column);
                        if (reader.hasNext()) {
                            reader.nextEvent(); // Don't read it - it's the character placeholder
                        }
                        writer.add(eventFactory.createCharacters(firstNonNull(value, "")));
                        if (reader.hasNext()) {
                            reader.nextEvent(); // Don't read it - it's the closing tag
                        }
                    }
                }
            } catch (XMLStreamException e) {
                throw new RuntimeException("Couldn't replace values for the playsql-field-macro", e);
            }
        });
    }


    private static ConversionContextOutputType convertOutputType(String outputType) {
        for (ConversionContextOutputType type : ConversionContextOutputType.values()) {
            // Can't use #valueOf() because type.value() returns the lowercase of itself
            if (Objects.equal(outputType, type.value()))
                return type;
        }
        return null;
    }

    private static <T> T displayValueOf(List<T> ts) {
        if (ts != null)
            if (ts.size() > 1) {
                return ts.get(1);
            } else {
                return ts.get(0);
            }
        return null;
    }

    private String getAttr(XMLEvent event, String attributeName) {
        Attribute attribute = event.asStartElement().getAttributeByName(new QName(attributeName));
        if (attribute != null) {
            return attribute.getValue();
        }
        return null;
    }

    private boolean hasCss(QName ATTR_CLASS, XMLEvent startElement, String clazz) {
        Attribute css = startElement.asStartElement().getAttributeByName(ATTR_CLASS);
        if (css != null) {
            String cssString = css.getValue();
            if (StringUtils.isNotBlank(cssString)) {
                List<String> cssClasses = Lists.newArrayList(cssString.split(" "));
                if (cssClasses.contains(clazz)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static class StackCounter {
        private LinkedList<Integer> stack = Lists.newLinkedList(); {
            stack.add(0);
        }
        public int currentLevel() {
            return stack.getLast();
        }
        public int increment(){
            Integer value = stack.removeLast();
            value++;
            stack.addLast(value);
            return value;
        }
        public int decrement(){
            Integer value = stack.removeLast();
            value--;
            stack.addLast(value);
            return value;
        }
        public void addLayer() {
            stack.addLast(0);
        }
        public int removeLayer() {
            return stack.removeLast();
        }
    }

    private String openXmlTransformer(String body, BiConsumer<XMLEventReader, XMLEventWriter> consumer) {
        StringWriter writer = null;
        XMLEventReader xmlEventReader = null;
        XMLEventWriter xmlWriter = null;
        try {
            writer = new StringWriter();
            xmlWriter = xmlOutputFactory.createXMLEventWriter(writer);
            xmlEventReader = xmlEventReaderFactory.createXmlFragmentEventReader(new StringReader(body));
            consumer.accept(xmlEventReader, xmlWriter);
            xmlWriter.flush();
            return writer.toString();
        } catch (XMLStreamException e) {
            return e.getMessage() + " <br>\n" + body;
        } finally {
            closeQuietly(writer);
            closeQuietlyXml(xmlEventReader);
            closeQuietlyXml(xmlWriter);
        }
    }

    protected static void closeQuietly(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                // Do nothing
            }
        }
    }

    protected static void closeQuietlyXml(XMLEventReader xml) {
        if (xml != null) {
            try {
                xml.close();
            } catch (XMLStreamException e) {
                // Do nothing
            }
        }
    }

    protected static void closeQuietlyXml(XMLEventWriter xml) {
        if (xml != null) {
            try {
                xml.close();
            } catch (XMLStreamException e) {
                // Do nothing
            }
        }
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.RICH_TEXT;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }

}
