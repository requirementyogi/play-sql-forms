package com.playsql.forms;


import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.util.HtmlUtil;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.Maps;
import com.playsql.spi.utils.PlaySqlUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Macro for panels, to display linked records.
 * Just a placeholder: Replacement is performed at runtime.
 *
 * Copyright 2015 Adrien Ragot - See LICENSE.txt
 */
public class PanelMacro implements Macro {

    final private static Logger LOG = LoggerFactory.getLogger(FormMacro.class);

    public final SoyTemplateRenderer soyTemplateRenderer;

    public PanelMacro(SoyTemplateRenderer soyTemplateRenderer) {
        this.soyTemplateRenderer = soyTemplateRenderer;
    }

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context) {
        Map<String, Object> map = Maps.newHashMap();
        map.putAll(parameters);
        String width = PlaySqlUtils.firstNonNull(parameters.get("width"), "100%");
        if (width != null && !FormMacro.WIDTH_VALIDATION.matcher(width).matches()) {
            width = null;
        }
        map.put("width", width);
        map.put("targetSpreadsheet", parameters.get("spreadsheetId"));
        map.put("cardLayout", !PlaySqlUtils.equal(parameters.get("cardLayout"), "false"));
        map.put("htmlUtil", new HtmlUtil());
        map.put("bodyHtml", body);

        try {
            return soyTemplateRenderer.render("com.playsql.playsql-forms-plugin:soy-templates", "Confluence.Templates.PlaySQL.formPanel", map);
        } catch (SoyException e) {
            LOG.info("Error while rendering Play SQL's 'panel' macro", e);
            return "Error while rendering Play SQL's 'panel' macro: " + e.getMessage();
        }
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.RICH_TEXT;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }

}
