/**
 * This mostly defines how the spreadsheetId is managed in the Macro Browser.
 * Copyright 2015 Adrien Ragot - See LICENSE.txt
 */
(function($){

    window.PlaySQL = window.PlaySQL || {};
    if (window.PlaySQL.MacroHelpers) return;
    window.PlaySQL.MacroHelpers = {
        /** Returns overrides to be used in:
         *
         * ``AJS.MacroBrowser.setMacroJsOverride("playsql-form", jsOverrides);``
         *
         * @param customOverrides An associative array with the field name as a key and
         *                        a function(params, options) as a value. Only overrides "string"-typed fields.
         *                        The default value is:
         *                        ``return AJS.MacroBrowser.ParameterFields["string"](params, options);``
         * @param onValidSpreadsheetSelected a function(contextKey, spreadsheetId) triggered when a non-null
         *                        spreadsheet is selected. If and only if the function returns true or undefined,
         *                        the preview is refreshed.
         */
        getPlaySQLOverrides : function(customOverrides, onValidSpreadsheetSelected){
            var readonly = false;
            /**
             * Fill the 'spreadsheetId' combo box with the list of spreadsheets.
             */
            function fillValues() {
                var context = PlaySQL.SPI.contextForPage,
                    url = PlaySQL.SPI.baseUrl(PlaySQL.SPI.contextForPage, {
                        recentlyViewed: true
                    });
                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: "json"
                }).done(function(data, status, jqXHR){
                    var $select = $("#macro-param-spreadsheetId"),
                        originalValue = $select.val(),
                        mode = jqXHR.getResponseHeader("X-MODE");
                    $select.empty();
                    addValue($select, ["undefined", "Select a value"]);
                    for (var i = 0 ; i < data.length ; i++) {
                        addValue($select, data[i]);
                    }
                    setSpreadsheetId($select, originalValue);

                    readonly = (mode != "READ_WRITE");
                    onSpreadsheetChanged();
                }).fail(function(data){
                    AJS.logError("Can't fetch the list of queries for " + PlaySQL.SPI.contextForPage, data);
                    var $error = $("#macro-param-div-queryId div.error");
                    if ($error.size() == 0) {
                        $error = $("<div>", {
                            "class" : "error macro-param-desc"
                        }).appendTo($("#macro-param-div-queryId"));
                    }
                    $error.text("Can't fetch the list of queries for this space");
                    try {
                        var data2 = $.parseJSON(data.responseText);
                        var message = data2.message;
                        if (message) {
                            message = message.replace(/^[a-z.A-Z]+Exception: /, "");
                            $error.text("Can't fetch the list of queries for this space: " + message);
                        }
                    } catch (err) {
                        // The text is already in the div
                    }

                });
            }
            /**
             * Add one value to the combo-box
             */
            function addValue($select, data) {
                var type = data[2],
                    $target = $select;
                // types 'undefined' and 'SPREADSHEET' don't use an optgroup
                if (!!type) {
                    $target = $select.find("optgroup[data-key=" + type + "]");
                    if ($target.size() == 0) {
                        var label = type == "JOINT_TABLE" ? "Joint Tables"
                                : type == "SPREADSHEET" ? "Spreadsheets"
                                : type == "RECENTLY_VIEWED" ? "Recently viewed"
                                : type;
                        $select.append($("<optgroup>", {
                            "data-key": type,
                            "label": label
                        }));
                        $target = $select.find("optgroup[data-key=" + type + "]");
                    }
                }

                $target.append(AJS.$("<option/>", {
                    value: data[0],
                    text: data[1],
                    "data-type": type
                }));
            }
            /**
             * Sets the combo-box to its value, and create it if necessary
             */
            function setSpreadsheetId($select, id) {
                $select.val(id);
                if (id != undefined && $select.val() !== id) {
                    addValue($select, [id, "Current Spreadsheet (" + id + ")"]);
                    $select.val(id);
                }
            }
            /**
             * Triggered when the value of the combo-box changes
             */
            function onSpreadsheetChanged() {
                var spreadsheetIdField = AJS.MacroBrowser.fields["spreadsheetId"],
                    spreadsheetId = spreadsheetIdField.getValue(),
                    spreadsheetName = $("#macro-param-spreadsheetId option[value='" + spreadsheetId + "']").text(),
                    oldSpreadsheetId = spreadsheetIdField.loadedSpreadsheetId,
                    spaceKey = AJS.Meta.get("space-key"),
                    proceed = true;
                if (spreadsheetId === oldSpreadsheetId) {
                    return;
                }
                spreadsheetIdField.loadedSpreadsheetId = spreadsheetId;
                // Update the name, to be user-friendly
                AJS.MacroBrowser.fields["spreadsheetName"].setValue(spreadsheetName);
                // In IE8, make sure the MacroBrowser knows required fields have values
                AJS.MacroBrowser.paramChanged();

                if (readonly) {
                    var $replaceAllParams = $("#macro-param-div-spreadsheetId");
                    $replaceAllParams.empty().html("This space is read-only. You cannot have Spreadsheets in this space.");
                } else if (spreadsheetId == undefined || spreadsheetId == "undefined") {
                    var $description = $("#macro-param-div-spreadsheetId .macro-param-desc"),
                        $createLink = $("<a></a>", {
                                href : PlaySQL.SPI.contextPath + "/playsql-base-plugin/index.action?" + $.param({key: spaceKey}) + "#create-spreadsheet",
                                text : "or create a new spreadsheet...",
                                target: "_blank"
                            });
                    if ($description.size() == 0) {
                        $description = $("#macro-param-div-spreadsheetId")
                            .append("<div class='macro-param-desc'></div>")
                            .find("div");
                    }
                    $description.empty().html($createLink);
                } else {
                    var $description = $("#macro-param-div-spreadsheetId .macro-param-desc"),
                        $macroFieldColumns = $("#macro-param-div-display-columns"),
                        $editLink = $("<a></a>", {
                            href : PlaySQL.SPI.contextPath + "/playsql-base-plugin/spreadsheet-editor.action?key=" + spaceKey + "&spreadsheetId=" + spreadsheetId,
                            text : "Edit the spreadsheet...",
                            target : "_blank"
                        });
                    if ($description.size() == 0) {
                        $description = $("#macro-param-div-spreadsheetId")
                            .append("<div class='macro-param-desc'></div>")
                            .find("div");
                    }
                    $description.unbind().empty().html($editLink);

                    if (onValidSpreadsheetSelected) {
                        proceed = onValidSpreadsheetSelected("space:" + spaceKey, spreadsheetId);
                        if (proceed == undefined) proceed = true;
                    }
                }
                if (proceed) {
                    $("#macro-browser-preview-link").click();
                }
            }

            // Now take care of the parameters & overrides we actually passed to getPlaySQLOverrides()
            customOverrides = $.extend({
                "spreadsheetId": function(params, options) {
                    var $paramDiv = $(Confluence.Templates.MacroBrowser.macroParameterSelect());
                    var $select = $("select", $paramDiv);

                    options.setValue = function(value) {
                        setSpreadsheetId($("#macro-param-spreadsheetId"), value);
                    }

                    fillValues();

                    options.onchange = onSpreadsheetChanged;

                    var field = AJS.MacroBrowser.Field($paramDiv, $select, options);
                    field.updateSpreadsheet = onSpreadsheetChanged;
                    return field;
                }
            }, customOverrides);

            return {
                "fields" : {
                    "string" : function(params, options) {
                        options = options || {};
                        if (!!customOverrides[params.name]) {
                            return customOverrides[params.name](params, options);
                        } else {
                            return AJS.MacroBrowser.ParameterFields["string"](params, options);
                        }
                    },
                    "enum" : function(params, options) {
                        options = options || {};
                        if (!!customOverrides[params.name]) {
                            return customOverrides[params.name](params, options);
                        } else {
                            return AJS.MacroBrowser.ParameterFields["enum"](params, options);
                        }
                    }
                },
                "beforeParamsSet": function(paramMap, inserting) {
                    if (!paramMap["context"]) {
                        paramMap["context"] = "space:" + AJS.Meta.get("space-key");
                    }
                    return paramMap;
                }
            }
        }
    };
})(AJS.$);