/* Copyright 2015 Adrien Ragot - See LICENSE.txt */
(function($) {
// This event is used by the renderers to declare themselves
AJS.bind("playsql-spreadsheet-init", function(event, api) {
    // We want the trigger after the renderers have been declared
    window.setTimeout(function(){

        function spreadsheetRest(endpoint, paths, queryParams) {
            var suffix = "";
            for (var i = 0 ; i < arguments.length ; i++) {
                var arg = arguments[i];
                if (i == arguments.length -1 && (typeof arg == "object")) {
                    suffix += "?" + $.param(arg);
                } else {
                    suffix += "/" + arg.replace(/^\/|\/$/g, "");
                }
            }

            return PlaySQL.SPI.baseUrl(suffix);
        }

        function findColumn(tableDef, columnName) {
            for (var i = 0; i < tableDef.columns.length; i++) {
                if (tableDef.columns[i].name == columnName) {
                    return tableDef.columns[i];
                }
            }
            return null;
        }

        var $macros = $(".playsql-form-macro");
        if ($macros.size() > 0) {
            $macros.each(function(){
                var $macro = $(this),
                    $form = $macro.find(".form-template"),
                    context = $macro.attr("data-macro-context"),
                    spreadsheetId = $macro.attr("data-macro-spreadsheet-id"),
                    initialState = $macro.attr("data-initial-state"),
                    recordId = $macro.find(".recordId").val(),
                    filterText = $macro.attr("data-filter-text");

                if (spreadsheetId == undefined || spreadsheetId == "") {
                    $form.text("Please select a spreadsheet for this form.");
                    return;
                }

                if ($macro.data("form-template") == undefined) {
                    $macro.data("form-template", $form.html());
                }

                /** Obviously it will only find one cell
                 * @param column the column name or the column definition
                 **/
                var $findColumnCells = function(column) {
                    var $placeholder = this.$placeholder,
                        columnName = typeof column == "string" ? column : column.name,
                        $fields = $placeholder.find('.playsql-form-field[data-column="' + columnName + '"]');
                    // Check the field isn't in a panel
                    $fields = $fields.filter(function() {
                        var $field = $(this),
                            isDirectDescendent = $field.parent().closest(".playsql-form-macro, .playsql-form-panel").is($placeholder.parent());
                        return isDirectDescendent;
                    });
                    return $fields;
                }

                var colOf = function ($cell) {
                    var column, columnName;
                    if ($cell instanceof $) {
                        columnName = $cell.attr("data-column");
                        column = findColumn(this.tableDef, columnName);
                    } else if (typeof $cell == "string") {
                        columnName = $cell;
                        column = findColumn(this.tableDef, columnName);
                    } else if ($.isNumeric($cell)) {
                        column = this.tableDef.column[$cell];
                    } else {
                        throw "colOf() didn't succeed to recognize the argument";
                    }
                    return column;
                };


                var getCell = function (jointSpreadsheetKey, row, col) {
                    var $cell, column;
                    if (arguments.length == 2) {
                        col = arguments[1];
                        row = arguments[0];
                        jointSpreadsheetKey = undefined;
                        if (typeof row === "string" && row.indexOf("-") != -1) {
                            throw "Not implemented: getCell('key-pk')";
                        }
                    } else if (arguments.length == 1) {
                        row = arguments[0];
                        jointSpreadsheetKey = undefined;
                    }
                    if (col == undefined) {
                        if (row == undefined) return undefined;
                        $cell = row; // We'll test later whether it's a jQuery instance.
                    } else if (jointSpreadsheetKey == undefined) {
                        // We should check 'row' here, but we'll assume it's the current row
                        column = this.colOf(col);
                        $cell = this.$placeholder.find('.playsql-form-field[data-column="' + column.name + '"]').first();
                    } else if (jointSpreadsheetKey != undefined) {
                        // We should check 'row' here, but we'll assume it's the current row
                        column = this.colOf(jointSpreadsheetKey, col);
                        $cell = this.$placeholder.find('.playsql-form-field[data-column="' + column.name + '"]').first();
                    }
                    if (!$cell instanceof $)
                        throw "Cell isn't a jQuery object: " + $cell;
                    if (!$cell.is(".playsql-form-field")) {
                        AJS.log("Cell isn't a field: ", $cell);
                        return undefined;
                    }

                    column = this.colOf($cell);
                    if (column == null) {
                        return null;
                    }
                    var renderer = window.PlaySQL.Spreadsheet.renderers[column.renderer];
                    var rendererInstance = renderer.forCell(this, $cell);
                    var viewRenderer = window.PlaySQL.Spreadsheet.renderers[column.viewRenderer || column.renderer];
                    var viewRendererInstance = viewRenderer.forCell(this, $cell);
                    rowId = this.getCurrentRecordValue({statusPK:true}, 0);

                    return {
                        $cell: $cell,
                        isColumnHeader: false,
                        column: column,
                        renderer: rendererInstance,
                        viewRenderer: viewRendererInstance,
                        rowId: rowId
                    };
                };
                var saveCell = function ($cell, val) {
                    var field = $cell.attr("data-column"),
                        columnDef = findColumn(spreadsheet.tableDef, field);
                    this.currentRecord[columnDef.index] = [val];
                };
                var sprayRequestPipe = function(){
                    // Do nothing by default.
                }
                var getCellMetadata = function($cell, path) {
                    var columnName = $cell.attr("data-column"),
                        rowMetadata = spreadsheet.getCurrentRecordValue({statusPosition: true}, 0);
                    if (rowMetadata) {
                        try {
                            rowMetadata = $.parseJSON(rowMetadata);
                        } catch (err) {
                            rowMetadata = undefined;
                        }
                    }
                    var cellMetadata = rowMetadata && rowMetadata["c_" + columnName];
                    if (cellMetadata && path) return cellMetadata[path];
                    return cellMetadata;
                }
                var saveCellMetadata = function($cell, name, value) {
                    throw "No cell metadata is supposed to be saved in a form";
                }
                /**
                 * Returns the value of the current record
                 * Syntax: getCurrentRecordValue()
                 * Syntax: getCurrentRecordValue({name: "firstcolumn"}, 0) The 0th element of the cell "firstcolumn"
                 * Syntax: getCurrentRecordValue({statusPK: true}, 0)
                 * Syntax: getCurrentRecordValue({statusPosition: true}, 0)
                 * @param cellElementIndex cells are made of an array of a few elements. This is the index of the
                 * element you want, probably 0.
                 * @return the value or undefined
                 */
                var getCurrentRecordValue = function(columnSpecifics, cellElementIndex) {
                    // currentRecord may be undefined when creating the record
                    if (!spreadsheet.currentRecord) return undefined;
                    if (typeof columnSpecifics == "string")
                        columnSpecifics = {name: columnSpecifics};
                    var keys = Object.keys(columnSpecifics);
                    for (var i = 0 ; i < spreadsheet.tableDef.columns.length ; i++) {
                        var column = spreadsheet.tableDef.columns[i],
                            matches = true;
                        for (var j = 0 ; j < keys.length ; j++) {
                            if (column[keys[j]] != columnSpecifics[keys[j]]) {
                                matches = false;
                                break;
                            }
                        }
                        if (matches) {
                            var element = spreadsheet.currentRecord[i];
                            // Not every cell is defined, for example when creating the record
                            if (typeof element != "undefined" && typeof cellElementIndex != "undefined")
                                element = element.length > cellElementIndex ? element[cellElementIndex] : undefined;
                            return element;
                        }
                    }
                    return undefined;
                }

                var getRecordIds = function($cell, useJointId) {
                    var cell = this.getCell($cell),
                        id = cell && cell.rowId,
                        jointId = cell && cell.column.formats["joint-id"],
                        jointTable = cell && cell.column.formats["joint-table"],
                        result = { id: id };
                    if (jointTable) {
                        // This cell is the joint id
                        result.jointId = cell.$cell.attr("data-value");
                        result.jointIdName = cell.column.name;
                        result.jointIdCell = cell;
                        result.jointTable = jointTable;
                        result.jointFk = cell.column.formats["joint-fk"];
                    } else if (jointId) {
                        // It's a joint cell
                        var idCell = spreadsheet.getCell(id, jointId);
                        if (!idCell) throw "There is no cell for the column " + jointId + " on the row " + $cell.closest("tr").attr("data-row-id");
                        var idValue = idCell.$cell.attr("data-value");
                        result.jointId = idValue;
                        result.jointIdName = jointId;
                        result.jointIdCell = idCell;
                        result.jointTable = idCell.column.formats["joint-table"];
                        result.jointFk = idCell.column.formats["joint-fk"];
                    }
                    return result;
                }

                var addJointRow = function(argument, callback) {
                    if (!spreadsheet.perms.hasPerm("Q_VIEW_EDIT")) return;
                    var $cell, cell;
                    if (argument instanceof $) {
                        $cell = argument;
                        cell = spreadsheet.getCell($cell);
                    } else if (argument && argument.$cell) {
                        cell = argument;
                        $cell = cell.$cell;
                    } else {
                        throw "Not implemented: addJointRow with argument " + argument;
                    }
                    window.PlaySQL.Messages.spin($cell).on(500, "The row is being created in the table on the server side. You cannot submit values until the row is ready.");
                    if (!cell || !cell.column.formats) {
                        window.PlaySQL.Messages.addMessage("error", "It seems like the properties of the joint table are incorrect. Please run the Joint Table wizard again.", "add-row");
                        return;
                    }
                    var jointTable = cell.column.formats["joint-table"],
                        jointFk = cell.column.formats["joint-fk"],
                        pk = cell.rowId;

                    // Ajax to get the ID
                    var url = PlaySQL.SPI.baseUrl(spreadsheet.context + "/" + jointTable + "/data"),
                        colNames = "POSITION," + jointFk,
                        data = [[[], [pk]]];
                    $.ajax({
                        url: url,
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify(data),
                        headers: {
                            "X-COL-NAMES": colNames
                        }
                    }).done(function(data, status, request){
                        window.PlaySQL.Messages.spin($cell).off();
                        if (data.insertedData.length != 1) {
                            window.PlaySQL.Messages.addMessage("error", "The row couldn't be created.", "add-row", jqXHR.responseText);
                        } else {
                            var insertedId = data.insertedData[0];
                            spreadsheet.currentRecord[cell.column.index] = [insertedId, insertedId];
                            cell.renderer.setOriginalValue(insertedId, insertedId);
                        }
                        spreadsheet.dispatchCellValues(data.data, data.tableDef);
                        callback && callback();
                    }).fail(function(jqXHR, error, reason){
                        window.PlaySQL.Messages.spin($cell).off();
                        window.PlaySQL.Messages.addMessage("error", "The row couldn't be created.", "add-row", jqXHR.responseText);
                    });
                }

                /** This is a stub representation of the spreadsheet,
                 * to trick the renderers */
                var spreadsheet = {
                    $placeholder: $form,
                    id: spreadsheetId,
                    context: context,
                    decimalSeparator: '.',
                    tableDef: undefined,
                    columns : [], // Empty. Please use tableDef.columns instead.
                    requestPipe: undefined,
                    $findColumnCells : $findColumnCells,
                    colOf : colOf,
                    getRecordIds : getRecordIds,
                    getCell : getCell,
                    getCellMetadata : getCellMetadata,
                    currentRecord : undefined,
                    getCurrentRecordValue : getCurrentRecordValue,
                    saveCell : saveCell,
                    saveCellMetadata : saveCellMetadata,
                    addJointRow: addJointRow,
                    dispatchCellValues: dispatchCellValues,
                    removeJointRow: function(){ throw "Not Implemented"; },
                    sprayRequestPipe: sprayRequestPipe,
                    // Not in the original spreadsheet definition
                    pagination: undefined,
                    data: undefined,
                    load: load,
                    mergeData : mergeData
                };
                window.PlaySQL.spreadsheet = spreadsheet;

                function parseIdFromHash() {
                    var hash = window.location.hash,
                        id = hash.match(/^#([0-9]+)/),
                        create = hash == "#create";
                    return id != null ? id[1] : create ? "create" : null;
                }

                if (recordId == "") {
                    recordId = parseIdFromHash();
                    if (recordId == "create") {
                        recordId = undefined;
                        initialState = "create";
                    }
                }
                renderForState(initialState, recordId, filterText);
                function goToHashLocation() {
                    var id = parseIdFromHash();
                    if (id == 'create') {
                        renderForState("create");
                    } else if (id != null) {
                        displayRecord("view", id);
                    }
                }
                function prevNextButtonHandler(e) {
                    e.preventDefault();
                    window.location.hash = $(this).attr("href");
                    goToHashLocation();
                }
                $form.parent().find(".browser-toolbar")
                    .on("click", ".prev", prevNextButtonHandler)
                    .on("click", ".next", prevNextButtonHandler)
                    .on("click", ".create", function(e){
                        e.preventDefault();
                        renderForState("create");
                    }).on("keydown", ".recordId", function(e){
                        if (e.which == 13) {
                            e.preventDefault();
                            var id = $(e.target).val();
                            window.location.hash = "#" + id;
                            goToHashLocation();
                        }
                    });

                /**
                 * For each $field,
                 * @param state
                 * @param $panel
                 * @param spreadsheet
                 * @param currentRecord
                 */
                function setValuesForPanel(state, $panel, spreadsheet, currentRecord) {
                    var isRootPanel = $panel.is(".playsql-form-macro");
                    if (isRootPanel) {
                        // This is required so getCellMetadata() return correct values
                        spreadsheet.currentRecord = currentRecord;
                    }
                    var $fieldsAndPanels = $panel.find('.playsql-form-field, .playsql-form-panel').filter(function(){
                            // Only keep direct descendants
                            return $(this).parent().closest(".playsql-form-macro, .playsql-form-panel > .contents").is($panel);
                        });
                    $fieldsAndPanels.each(function(){
                        var $field = $(this);
                        if ($field.is(".playsql-form-field")) {
                            var columnName = $field.attr("data-column"),
                                cell = spreadsheet.getCell($field);
                            if (cell == null) {
                                $field.text("Error: The field '" + columnName + "' is unknown in the table '" + spreadsheet.tableDef.name + "'.");
                                return;
                            }
                            var columnDef = cell.column,
                                value = currentRecord[columnDef.index];
                            var value0 = value[0] === undefined ? "" : value[0];
                            var value1 = value[1] === undefined ? "" : value[1];

                            if (state == 'edit' || state == 'create') {
                                cell.viewRenderer.setOriginalValue(value0, value1);
                                if (isRootPanel && spreadsheet.getCellMetadata(cell.$cell, "formula")) {
                                    cell.$cell.addClass("formula").attr("title", "This value is automatically set by a formula").tooltip({delayIn: 0});
                                } else {
                                    cell.$cell.removeClass("formula").removeAttr("title").removeAttr("original-title");
                                    cell.$cell.tipsy && cell.$cell.tipsy("disable");
                                }
                                cell.renderer.edit();
                                var $input = $field.find("input, textarea, div[contenteditable], select");
                                // Deactivate the 'blur' event
                                $input.off("blur");
                                // Mark it as an AUI field
                                $input.addClass("text");
                                if (state == 'create') $field.attr("data-value", "");
                            } else if (state == 'view' || state == 'viewOne') {
                                cell.viewRenderer.setOriginalValue(value0, value1);
                                cell.viewRenderer.parent.onColumnDisplay(spreadsheet, columnDef);
                            }
                        } else if ($field.is(".playsql-form-panel")) {
                            // It's a panel = it's a foreign key
                            $field.addClass("disabled").find(".message").text("(Please wait)");
                            $field.find("> .contents:not(:first)").remove();
                            if (state == 'create' || state == 'edit') {
                                $field.find(".message").text("This panel is disabled while creating or editing records.");
                                return;
                            }
                            var targetSpreadsheet = $field.attr("data-target-spreadsheet"),
                                targetColumn = $field.attr("data-target-column"),
                                localColumnName = $field.attr("data-column"),
                                localColumnDef = findColumn(spreadsheet.tableDef, localColumnName);
                            if (localColumnDef == null) {
                                $field.find("contents").text("Error: Could not find column with name '" + localColumnName + "'.");
                            }
                            var value = currentRecord[localColumnDef.index][1];
                            // AJAX query with WHERE clause, then recursive call
                            var whereClause = '"' + targetColumn + '" = ';
                            if (localColumnDef.type != 'INTEGER' && localColumnDef.type != 'BOOLEAN'
                             && localColumnDef.type != 'NUMERIC' && localColumnDef.type != 'DOUBLE') {
                                // Hopeless effort to sql-escape by doubling quotes. Not pirate-proof.
                                // But the only thing you'd sql-inject-attack is
                                // with a pirate value, you could call a stored procedure. It won't be possible
                                // to do two sql statements nor add an insert/truncate statement in between.
                                // TODO check: If someone can save a weird cell value, can't they already call a
                                // stored procedure?
                                whereClause += "'" + value.replace("'", "''") + "'";
                            } else {
                                whereClause += value;
                            }
                            var url = spreadsheetRest(context, targetSpreadsheet, "/data", { whereClause: whereClause });
                            $.ajax({
                                url: url,
                                type: 'GET',
                                contentType: "application/json; charset=utf-8",
                                timeout: 180000,
                                cache: false
                            }).done(function(data, status, request) {
                                var tableDef = data.tableDef,
                                    // The easiest way to inherit the methods
                                    innerSpreadsheet = $.extend({}, spreadsheet, {
                                        tableDef: tableDef,
                                        perms: window.PlaySQL.Helpers.parsePerms(tableDef.perms)
                                    });
                                for (var i = 0 ; i < tableDef.columns.length ; i++) {
                                    tableDef.columns[i].index = i;
                                    tableDef.columns[i].$placeholder = spreadsheet.$placeholder;
                                }
                                if (!data.data || data.data.length == 0) {
                                    $field.find(".message").text("There is no linked record.");
                                    return;
                                }
                                for (var i = 0 ; i < data.data.length ; i++) {
                                    var record = data.data[i],
                                        $clone = $field.find(".contents:last");
                                    if (i != 0) {
                                        $clone = $clone.clone().insertAfter($clone);
                                    }
                                    setValuesForPanel(state, $clone, innerSpreadsheet, record);
                                }
                                // We only remove the class in the right case
                                $field.removeClass("disabled");
                            }).fail(function(jqXHR, error, reason){
                                var message = "The field couldn't be loaded"
                                    + " (HTTP status code: " + jqXHR.status + ") - "
                                    + jqXHR.responseText;
                                AJS.logError("PlaySQL error: " + message + "\n" + jqXHR.responseText);
                                $field.find(".message").text(message);
                            });
                        }
                    });
                }

                /** Displays the record based on spreadsheet.data[i]
                 * @param state Either 'view', 'viewOne', 'edit'
                 * @param id
                 */
                function displayRecord(state, id) {
                    var $fields = $form.find('.playsql-form-field'),
                        $messages = $form.find(".form-messages"),
                        data = spreadsheet.data,
                        currentRecord, nextRecord, prevRecord,
                        currentRecordPosition,
                        total = spreadsheet.pagination && spreadsheet.pagination.count || data.length;
                    if (id != undefined) {
                        for (var i = 0 ; i < data.length ; i++) {
                            if (data[i][0][0] == id) {
                                setCurrentRecord(i);
                                break;
                            }
                        }
                    }
                    if (currentRecord == undefined) {
                        if (data.length > 0) {
                            function setDefaultRecord() {
                                if (getCurrentRecordValue({statusPK: true}) == undefined) {
                                    displayRecord(state, spreadsheet.data[0][0][0]);
                                }
                            }
                            if (id != undefined && id != '') {
                                if (spreadsheet.pagination && data.length < spreadsheet.pagination.count) {
                                    var offset = data.length;
                                    spreadsheet.load(null, {offset: offset}, function(){
                                        if (spreadsheet.data.length <= offset) {
                                            AJS.messages.warning($messages, {
                                                //title: "The record with id '" + id + "' couldn't be found",
                                                closeable: true,
                                                body: "The record with id '" + id + "' couldn't be found and not all records could be fetched."
                                            });
                                            setDefaultRecord();
                                        } else {
                                            displayRecord(state, id);
                                        }
                                    });
                                } else {
                                    AJS.messages.warning($messages, {
                                        //title: "The record with id '" + id + "' couldn't be found",
                                        closeable: true,
                                        body: "The record with id '" + id + "' couldn't be found"
                                    });
                                    setDefaultRecord();
                                }
                                return;
                            } else {
                                setCurrentRecord(0);
                            }
                        } else {
                            AJS.messages.warning($messages, {
                                //title: "The record with id '" + id + "' couldn't be found",
                                closeable: true,
                                body: "There are no records in this spreadsheet"
                            });
                            $fields.text("-");
                            return;
                        }
                    }
                    function setCurrentRecord(position) {
                        currentRecord = data[position];
                        if (position > 0) { prevRecord = data[position-1]; }
                        if (position < data.length - 1) { nextRecord = data[position+1]; }
                        currentRecordPosition = position;
                        id = currentRecord[0][0];

                        spreadsheet.currentRecord = currentRecord;
                    }
                    if (state == "edit") {
                        // If there's a joint table, create the foreign records
                        for (var i = 0 ; i < spreadsheet.tableDef.columns.length ; i++) {
                            var column = spreadsheet.tableDef.columns[i];
                            if (column.formats["joint-table"]) {
                                var $cell = spreadsheet.$findColumnCells(column),
                                    recordIds = spreadsheet.getRecordIds($cell, true);
                                if ($cell.size() > 0 && recordIds.id && recordIds.jointIdName && !recordIds.jointId) {
                                    spreadsheet.addJointRow($cell, function () {
                                        displayRecord(state, id);
                                    });
                                    return;
                                }
                            }
                        }
                    }

                    // Update the record browser
                    var $prev = $form.parent().find(".browser-toolbar .prev"),
                        $next = $form.parent().find(".browser-toolbar .next"),
                        $recordId = $form.parent().find(".browser-toolbar .recordId"),
                        $counter = $form.parent().find(".browser-toolbar .counter");
                    if (prevRecord != undefined) {
                        $prev.removeClass("hidden").attr("href", "#" + prevRecord[0][0]);
                    } else {
                        $prev.addClass("hidden");
                    }
                    if (nextRecord != undefined) {
                        $next.removeClass("hidden").attr("href", "#" + nextRecord[0][0]);
                    } else {
                        if (spreadsheet.pagination && data.length < spreadsheet.pagination.count) {
                            // Load another page
                            $next.removeClass("hidden").addClass("disabled");
                            var offset = data.length;
                            spreadsheet.load(null, { offset: offset }, function(){
                                if (spreadsheet.data.length > offset) {
                                    nextRecord = spreadsheet.data[offset];
                                    $next.removeClass("disabled").attr("href", "#" + nextRecord[0][0]);
                                } else {
                                    $next.removeClass("disabled").addClass("hidden");
                                }
                            });
                        } else {
                            $next.addClass("hidden");
                        }
                    }
                    $recordId.val(spreadsheet.getCurrentRecordValue({statusPK: true}, 0));
                    $counter.text((currentRecordPosition + 1) + " of " + total);

                    setValuesForPanel(state, $form.parent(), spreadsheet, currentRecord);

                    updateButton(state, id);
                }

                function updateButton(state, id) {
                    var $button = $form.find(".save-record").removeClass("aui-disabled playsql-disabled aui-button-primary"),
                        $cancelButton = $form.find(".cancel-record").addClass("hidden");
                    if ($button.length > 0) {
                        var saveRecordHandler = function(e) {
                            e.preventDefault();
                            saveRecord(state, id);
                            return false;
                        }
                        var cancelRecord = function (e) {
                            e.preventDefault();
                            displayRecord("view", id);
                            return false;
                        };
                        if (state == 'create') {
                            if (spreadsheet.perms.hasPerm('ADD_ROWS')) {
                                $button.removeClass("hidden").off("click").click(saveRecordHandler).text("Create");
                            }
                        } else if (state == 'edit') {
                            if (spreadsheet.perms.hasPerm('EDIT_CELLS') || spreadsheet.perms.hasPerm('Q_VIEW_EDIT')) {
                                $button.removeClass("hidden").addClass("aui-button-primary").off("click").click(saveRecordHandler).text("Save");
                                $cancelButton.removeClass("hidden").off("click").on("click", cancelRecord);
                            }
                        } else if (state == 'view' || state == 'viewOne') {
                            if (spreadsheet.perms.hasPerm('EDIT_CELLS') || spreadsheet.perms.hasPerm('Q_VIEW_EDIT')) {
                                $button.removeClass("hidden").off("click").click(function(e){
                                    e.preventDefault();
                                    displayRecord('edit', id);
                                }).text("Edit");
                            }
                        }
                    }
                }

                /** Merges the data into the spreadsheet's data.
                 * @param id the id of the current record. Undefined is allowed.
                 * @param tableName the definition of the table, in the case of a joint table
                 * */
                function mergeData(data, id, tableDef) {
                    var tableName = tableDef && tableDef.name,
                        jointSpreadsheetKey = tableName && tableName != spreadsheet.id ? tableName : undefined,
                        jointIdColumn,
                        /** Index of the ID column. Should be 0, except when it's a joint id*/
                        idColumnIndex = 0;
                    if (!spreadsheet.data || spreadsheet.data.length == 0) {
                        // If we get data from a joint table, there should already be data from the current ss
                        if (!jointSpreadsheetKey) spreadsheet.data = data;
                    } else {
                        if (jointSpreadsheetKey) {
                            for (var j = 0 ; j < spreadsheet.tableDef.columns.length ; j++) {
                                if (spreadsheet.tableDef.columns[j].formats["joint-table"] == tableName) {
                                    jointIdColumn = spreadsheet.tableDef.columns[j];
                                    idColumnIndex = jointIdColumn.index;
                                    id = undefined;
                                    break;
                                }
                            }
                            if (!jointIdColumn) throw "Joint column not found for " + tableDef.name;
                        } else {
                            if (!id) id = spreadsheet.currentRecord && spreadsheet.currentRecord[0] && spreadsheet.currentRecord[0][0];
                        }
                        for (var i = 0 ; i < data.length ; i++) {
                            var rowFromRest = data[i],
                                found = false;
                            // Reintegrate this data into spreadsheet.data
                            for (var j = 0 ; j < spreadsheet.data.length ; j++) {
                                if (spreadsheet.data[j][idColumnIndex][0] == rowFromRest[0][0]) {
                                    // We're on the right row. Now should we just replace the row or replace each
                                    // cell one by one? (We do the second one if it's a joint table, because we need
                                    // to match the column names)
                                    if (jointSpreadsheetKey) {
                                        // We're in a joint table. Only assign the half-row, cell by cell, matching the column names
                                        for (var ssColIndex = 0 ; ssColIndex < spreadsheet.tableDef.columns.length ; ssColIndex++ ) {
                                            for (var jointColIndex = 0 ; jointColIndex < tableDef.columns.length ; jointColIndex++ ) {
                                                var isCorrectColumnName = tableDef.columns[jointColIndex].name == (spreadsheet.tableDef.columns[ssColIndex].formats["joint-column"] || spreadsheet.tableDef.columns[ssColIndex].name),
                                                    isCorrectJointTable = jointIdColumn.name == spreadsheet.tableDef.columns[ssColIndex].formats["joint-id"];
                                                if (isCorrectJointTable && isCorrectColumnName) {
                                                    spreadsheet.data[j][ssColIndex] = rowFromRest[jointColIndex];
                                                }
                                            }
                                        }
                                    } else {
                                        // The full row can be assigned
                                        spreadsheet.data[j] = rowFromRest;
                                    }
                                    found = true;
                                    break;
                                }
                            }
                            if (!found) {
                                spreadsheet.data.push(rowFromRest);
                            }
                            // Also set the current record, if it's the right one
                            if (id == rowFromRest[0][0]) {
                                spreadsheet.currentRecord = rowFromRest;
                            }
                        }
                    }
                }
                function dispatchCellValues(data, tableDef) {
                    mergeData(data, undefined, tableDef);
                }

                function load(urlSuffix, urlParameters, callback) {
                    var url = spreadsheetRest(context, spreadsheetId, "/data") + (!!urlSuffix ? urlSuffix : "")
                        + "?" + $.param(urlParameters);
                    $.ajax({
                        url: url,
                        type: 'GET',
                        contentType: "application/json; charset=utf-8",
                        timeout: 180000,
                        cache: false
                    }).done(function(data, status, request) {
                        var tableDef = data.tableDef;
                        spreadsheet.pagination = data.pagination;
                        spreadsheet.tableDef = tableDef;
                        spreadsheet.perms = window.PlaySQL.Helpers.intersect(window.PlaySQL.Helpers.parsePerms(tableDef.perms),
                            ["VIEW", "ADD_ROWS", "EDIT_CELLS", "REMOVE_ROWS", "Q_VIEW", "Q_VIEW_EDIT"]);
                        if (spreadsheet.perms.hasPerm('ADD_ROWS')) {
                            $form.parent().find(".browser-toolbar .create").removeClass("hidden");
                        }
                        for (var i = 0 ; i < tableDef.columns.length ; i++) {
                            tableDef.columns[i].index = i;
                        }
                        spreadsheet.mergeData(data.data);
                        callback && callback();
                    }).fail(function(jqXHR, error, reason){
                        var message = "The form couldn't be loaded"
                            + " (HTTP status code: " + jqXHR.status + ") - "
                            + jqXHR.responseText;
                        AJS.logError("PlaySQL error: " + message + "\n" + jqXHR.responseText);
                        $form.text(message);
                    });
                }

                /**
                 * Performs a REST request to get the data and calls displayRecord().
                 * In the case of 'view', it fetches many records and displays the 'id' passed as a parameter.
                 * @param state Either 'create', 'viewOne', 'view', 'edit'
                 * @param id
                 * @param filterText
                 */
                function renderForState(state, id, filterText) {
                    var urlSuffix = null,
                        urlParameters = {};
                    if (state == 'create') {
                        urlParameters.limit = 0;
                    } else if (state == 'viewOne') {
                        urlSuffix = "/" + id;
                        urlParameters.limit = 1;
                    } else {
                        var whereClause = window.PlaySQL.Helpers.replaceBuiltInVariables(filterText);
                        if (whereClause)
                            urlParameters.whereClause = whereClause;
                    }

                    spreadsheet.load(urlSuffix, urlParameters, function(){
                        if (state == 'create') {
                            // Create an artificial record from the tableDef definition + the default values of fields
                            var record = [];
                            for (var i = 0 ; i < spreadsheet.tableDef.columns.length ; i++) {
                                var $field = $form.find('.playsql-form-field[data-column="' + spreadsheet.tableDef.columns[i].name + '"]').first();
                                if ($field.length > 0) {
                                    var defaultValue = $field.attr('data-default-value');
                                    defaultValue = window.PlaySQL.Helpers.replaceBuiltInVariables(defaultValue);
                                    record.push([undefined, defaultValue]);
                                } else {
                                    record.push([]);
                                }
                            }
                            setValuesForPanel("create", $form.parent(), spreadsheet, record);
                            $form.parent().find(".recordId").val("");
                            updateButton("create");
                        } else {
                            displayRecord(state, id);
                        }
                    });
                }

                function saveRecord(state, id) {
                    var $fields = $form.find(".playsql-form-field"),
                        $messages = $form.find(".form-messages"),
                        $submitButton = $form.find(".save-record");
                    if ($submitButton.hasClass("playsql-disabled")) {
                        return;
                    }
                    $messages.empty();
                    // Empty the current record (but keep the ID value), so saving will only push values of existing fields
                    spreadsheet.currentRecord = spreadsheet.currentRecord[0] ? [spreadsheet.currentRecord[0]] : [];
                    $fields.each(function(){
                        // Save it
                        var $field = $(this),
                            isDirectDescendant = $field.parent().closest(".playsql-form-macro, .playsql-form-panel").is($form.parent());
                        if (!isDirectDescendant) {
                            $field.removeClass("invalid");
                            return;
                        }
                        if ($field.hasClass("formula")) return;
                        $field.filter(".saving").removeClass("saving invalid").addClass("editing");
                        var columnDef = findColumn(spreadsheet.tableDef, $field.attr("data-column"));
                        if (columnDef == null) {
                            $field.addClass("invalid");
                            return;
                        }
                        var renderer = window.PlaySQL.Spreadsheet.renderers[columnDef.renderer];
                        var rendererInstance = renderer.forCell(spreadsheet, $field);
                        // And because we're missing a bit of abstraction here... we need to override '$cell.closest('tr')'
                        // to return the row id.
                        $field.closest_original = $field.closest;
                        $field.closest = function(selector) {
                            var $result = $field.closest_original(selector);
                            if (selector == "tr") {
                                $result.attr_original = $result.attr;
                                $result.attr = function(name, value) {
                                    if (name == 'data-row-id' && value == undefined) {
                                        return "I'm not null";
                                    }
                                    return $result.attr_original(name, value);
                                }
                            }
                            return $result;
                        }
                        $field.html = function(){/* Noop */ return $field;};
                        rendererInstance.saveAndView();
                        if ($field.attr("data-required") == 'true') {
                            var value = spreadsheet.getCurrentRecordValue(columnDef.name, 0);
                            if (value === '' || value === null) {
                                $field.addClass("invalid").attr("title", "This field is required");
                            }
                        }
                    });
                    if ($fields.filter(".invalid").size() > 0) {
                        AJS.messages.error($messages, {
                            title: "The form couldn't be submitted",
                            closeable: true,
                            body: "Some fields are invalid"
                        });
                        $fields.filter(".invalid[title='']").each(function(){
                            $(this).attr("title", "Please check the type of the field");
                        });
                        return;
                    }
                    /** 'queue' contains the records to save
                     * queue[jointIdCol].tableName = tableName, or null if current table
                     * queue[jointIdCol].id = 3
                     * queue[jointIdCol].idCol = the column definition
                     * queue[jointIdCol].columns = [col1, col2, ...]
                     * queue[jointIdCol].record = [[cell1], [cell2], ...]
                     */
                    var queue = {};
                    for (var i = 0 ; i < spreadsheet.tableDef.columns.length ; i++) {
                        var column = spreadsheet.tableDef.columns[i],
                            recordValue = spreadsheet.currentRecord[i];
                        if (column.statusPK) {
                            queue[undefined] = queue[undefined] || {columns: [], record: []};
                            queue[undefined].tableName = spreadsheet.id;
                            queue[undefined].idCol = column;
                            queue[undefined].id = recordValue && recordValue[0];
                        } else if (!column.statusPosition && recordValue) {
                            recordValue = recordValue[0];
                            var jointId = column.formats["joint-id"],
                                jointColumn = column.formats["joint-column"],
                                jointTable = column.formats["joint-table"];
                            if (jointTable) {
                                // It's the ID of the table! It's the ID of the table!
                                queue[column.name] = queue[column.name] || {columns: [], record: []};
                                queue[column.name].idCol = column;
                                queue[column.name].tableName = jointTable;
                                queue[column.name].id = recordValue;
                            } else {
                                queue[jointId] = queue[jointId] || {columns: [], record: []};
                                queue[jointId].columns.push(jointColumn || column.name);
                                queue[jointId].record.push([recordValue]);
                            }
                        }
                    }
                    if (!spreadsheet.perms.hasPerm('EDIT_CELLS')) {
                        delete queue[undefined];
                    }
                    if (!spreadsheet.perms.hasPerm('Q_VIEW_EDIT')) {
                        return; // Why are we here then!?
                    }

                    $submitButton.addClass("playsql-disabled aui-disabled");

                    // We don't transform this into a 0..length loop because
                    // keys include "undefined" (for the current table) and other
                    // names.
                    for (var i in queue) {
                        if (!queue.hasOwnProperty(i)) continue;
                        var queueItem = queue[i];
                        /** This is where we send data to the back-end:
                         * - the optional header X-COL-NAMES contains a comma-separated list of columns, example: POSITION,NAME
                         * - the contents is an array of values. Values are wrapped in an array, example: [[1], ["John"]].
                         * The returned data is an array of rows which have been modified, each row is an array
                         * of cells, each cell has 2 elements: the front-end value (after applying the formula) and
                         * the back-end value (the raw back-end data). Exemple:
                         * [[[1, 1], ["John", "<a href=...>John</a>"]]] (one row, 2 columns)
                         */
                        if (queueItem.columns.length == 0 && queueItem.record.length == 0 && state == 'edit') {
                            AJS.messages.success($messages, {
                                title: "Saved!",
                                closeable: true,
                                body: "Thank you! You didn't change any data."
                            });
                            // Notice that even if we submitted a joint table, the ID below is just the ID of the
                            // original column.
                            window.location.hash = "#" + id;
                            displayRecord("view", id);
                        } else {
                            var url = spreadsheetRest(spreadsheet.context, queueItem.tableName, "/data");
                            if (state == 'edit') url += "/" + queueItem.id;

                            $.ajax({
                                url: url,
                                type: state == 'create' ? 'POST' : 'PUT',
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                data: JSON.stringify(state == 'create' ? [queueItem.record] : queueItem.record),
                                headers: {
                                    "X-COL-NAMES": queueItem.columns.join(",")
                                }
                            }).done(function (data, status, request) {
                                //$submitButton.removeClass("aui-disabled playsql-disabled");
                                AJS.messages.success($messages, {
                                    title: "Saved!",
                                    closeable: true,
                                    body: "Thank you! Your data was saved."
                                });

                                spreadsheet.mergeData(data.data, undefined, data.tableDef);

                                // In edit mode, the REST endpoint returns all data
                                if (state == 'edit' || state == 'create') {
                                    // Since PLAYSQL-127, all data and their dependent rows are returned, so the ID of the current
                                    // row is defined by the id we were editing, or the insertedId.
                                    if (data.tableDef.name == spreadsheet.id && state == 'create') id = data.insertedData[0];
                                    // Notice that even if we submitted a joint table, the ID below is just the ID of the
                                    // original column.
                                    window.location.hash = "#" + id;
                                    displayRecord("view", id);
                                }
                            }).fail(function (jqXHR, error, reason) {
                                $submitButton.removeClass("aui-disabled playsql-disabled");
                                var message = typeof jqXHR.responseText != "undefined" ? jqXHR.responseText : "";
                                try {
                                    message = JSON.parse(message).message;
                                } catch (err) {
                                    // Alright, it's not json
                                }
                                if (message.length > 200) {
                                    message = message.substr(message, 0, 200);
                                }
                                AJS.messages.error($messages, {
                                    title: "The form couldn't be saved by the server",
                                    closeable: true,
                                    body: AJS.escapeHtml(message)
                                });
                            });
                        }
                    }
                }
            });
        }
    }, 0);
});
})(AJS.$);