This is the source of the [Atlassian Confluence](atlassian.com/software/confluence/) add-on published on the Marketplace as [Play SQL Forms](https://marketplace.atlassian.com/plugins/com.playsql.playsql-forms-plugin).

## Objectives

Demonstrate how to use the [Play SQL Spreadsheets](https://marketplace.atlassian.com/plugins/com.playsql.playsql-plugin) API to create your own project.

This particular project defines macros so users can setup forms.

## Starting

When you perform Atlassian development, it's easier to install the [Atlassian SDK](https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project).
To start the project, first download Play SQL Spreadsheets:


    # Download the dependency. Please change the version number for the laatest version ID.
    mkdir target
    cd target
    wget https://marketplace.atlassian.com/download/plugins/com.playsql.playsql-plugin/version/272
    mv 272 playsql-plugin-2.6.obr

    # Install the obr you've just downloaded
    mvn install:install-file -Dfile=playsql-plugin-2.6.obr -DgroupId=com.playsql -DartifactId=playsql-plugin -Dversion=2.6 -Dpackaging=obr

    # Eventually, install the sub-artifacts
    unzip playsql-plugin-2.6.obr
    mvn install:install-file -Dfile=playsql-plugin-2.6.jar -DgroupId=com.playsql -DartifactId=playsql-plugin -Dversion=2.6 -Dpackaging=jar
    mvn install:install-file -Dfile=dependencies/playsql-base-plugin-2.6.jar -DgroupId=com.playsql -DartifactId=playsql-base-plugin -Dversion=2.6 -Dpackaging=jar
    cd ..

    # Compile
    mvn install

    # Launch Confluence
    mvn amps:debug

    # To reinstall the plugin each time you make a modification:
    mvn confluence:cli
    # Then the command is `pi` (like "Plugin Install" or "Piscine Molitor Patel")

Works with `mvn amps:run` too. DO NOT use `mvn confluence:run`.

## License Apache 2.0

As described in http://www.apache.org/licenses/LICENSE-2.0.html and in LICENSE.txt.