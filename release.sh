#!/bin/bash

echo
echo "Usage ./release.sh 2.0-beta-2 2.0-SNAPSHOT"
echo

set -e -u
ROOT_NAME=$(xpath pom.xml "/project/artifactId/text()" 2>/dev/null)
VER="$1"
NEXT_VER="$2"
CURRENT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
CURRENT_COMMIT=$(git rev-parse HEAD)
LAST_COMMIT=$(git log -1 --pretty=oneline)
HAS_MVN=$(echo $LAST_COMMIT | grep "maven-release-plugin" || true)
HAS_NO_RELEASE=$(grep "NO_RELEASE" -l -RI * | grep -v "/target/" | wc -l)

echo
echo
echo "Going to release $VER and reset to $NEXT_VER"
echo "Project root is: $ROOT_NAME"
echo "Current branch is $CURRENT_BRANCH and current commit $CURRENT_COMMIT"
echo "Has mvn: $HAS_MVN"
echo

if [ -z "$ROOT_NAME" ] ; then
	echo "Root name empty"
	exit 1
fi

if [ $HAS_NO_RELEASE -ne 1 ] ; then
    echo "There is a comment somewhere with the tag NO_RELEASE. Please check."
    grep "NO_RELEASE" -l -RI *
fi

echo "Press any key to continue"
read

echo
echo
echo "Changing version to $VER"
echo
mvn versions:set -DgenerateBackupPoms=false -DnewVersion=$VER
mvn clean install
git commit -am "[auto] Set version to $VER"
git tag -a $ROOT_NAME-$VER -m "Release $VER"

# Save the artifacts
ls target/*.jar
cp target/*.jar ../releases/

echo
echo
echo "Changing version to $NEXT_VER"
echo
mvn versions:set -DgenerateBackupPoms=false -DnewVersion=$NEXT_VER
mvn clean install
git commit -am "[auto] Set version to $NEXT_VER"



echo
echo
echo "Pushing..."
echo
git push --tags

echo
echo
echo "Get cracking, now"
echo
